from .models import Lead
from .serializers import LeadSerializer
from rest_framework import viewsets, permissions

# Lead Viewset - help in creating a full CRUD functionality 
class LeadViewSet(viewsets.ModelViewSet):
    # queryset = Lead.objects.all() ## overide this
    permission_classes = [
        permissions.IsAuthenticated
    ]

    serializer_class = LeadSerializer

    def get_queryset(self):
        return self.request.user.leads.all()

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)
    